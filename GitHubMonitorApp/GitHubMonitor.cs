using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SlackBotMessages;
using SlackBotMessages.Models;
using System;
using System.IO;
using System.Threading.Tasks;

namespace GitHubMonitorApp
{
    public static class GitHubMonitor
    {
        [FunctionName("GitHubMonitor")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            var WebHookUrl = "https://hooks.slack.com/services/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";
            var WebHookUrl2 = "https://hooks.slack.com/services/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";

            log.LogInformation("Our GitHub Monitor processed an action.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var data = JsonConvert.DeserializeObject<Rootobject>(requestBody);

            SlackMessager.SendSlackMessage(data, WebHookUrl);
            SlackMessager.SendSlackMessage(data, WebHookUrl2);

            log.LogInformation(requestBody);

            return new OkResult();
        }
    }
}

public class SlackMessager {
    public static void SendSlackMessage(Rootobject data, string url)
    {

        var client = new SbmClient(url);

        Message message;

        if(data != null)
        {
            message = new Message("A new push was made to " 
                + data.project.name + " by " + data.commits[0].author.name)
                            .SetUserWithEmoji("Website", Emoji.Coffee);

            foreach (Commit c in data.commits)
            {
                message.AddAttachment(new Attachment()
                .AddField(c.message, "<" + c.url + "|" + c.id + ">"));
            }
            client.Send(message);
        }
    }
}

public class Rootobject
{
    public string object_kind { get; set; }
    public string event_name { get; set; }
    public string before { get; set; }
    public string after { get; set; }
    public string _ref { get; set; }
    public string checkout_sha { get; set; }
    public object message { get; set; }
    public int user_id { get; set; }
    public string user_name { get; set; }
    public string user_username { get; set; }
    public string user_email { get; set; }
    public string user_avatar { get; set; }
    public int project_id { get; set; }
    public Project project { get; set; }
    public Commit[] commits { get; set; }
    public int total_commits_count { get; set; }
    public Push_Options push_options { get; set; }
    public Repository repository { get; set; }
}

public class Project
{
    public int id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public string web_url { get; set; }
    public object avatar_url { get; set; }
    public string git_ssh_url { get; set; }
    public string git_http_url { get; set; }
    public string _namespace { get; set; }
    public int visibility_level { get; set; }
    public string path_with_namespace { get; set; }
    public string default_branch { get; set; }
    public string ci_config_path { get; set; }
    public string homepage { get; set; }
    public string url { get; set; }
    public string ssh_url { get; set; }
    public string http_url { get; set; }
}

public class Push_Options
{
}

public class Repository
{
    public string name { get; set; }
    public string url { get; set; }
    public string description { get; set; }
    public string homepage { get; set; }
    public string git_http_url { get; set; }
    public string git_ssh_url { get; set; }
    public int visibility_level { get; set; }
}

public class Commit
{
    public string id { get; set; }
    public string message { get; set; }
    public string title { get; set; }
    public DateTime timestamp { get; set; }
    public string url { get; set; }
    public Author author { get; set; }
    public string[] added { get; set; }
    public object[] modified { get; set; }
    public object[] removed { get; set; }
}

public class Author
{
    public string name { get; set; }
    public string email { get; set; }
}

